FROM openjdk
WORKDIR /usr/src/app
COPY build/libs/do-auth-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8082
ENTRYPOINT ["java","-jar","/usr/src/app/app.jar"]
