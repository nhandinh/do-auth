package com.codecamp.auth.repositories

import com.codecamp.common.domains.User
import com.codecamp.common.utils.AuthProvider
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email)

  Boolean existsByEmail(String email)

  Optional<User> findByEmailAndProvider(String email, AuthProvider provider)
}
