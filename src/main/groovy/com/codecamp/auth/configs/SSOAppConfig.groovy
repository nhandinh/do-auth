package com.codecamp.auth.configs

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = 'spring.security.oauth2.client.registration')
class SSOAppConfig {
  SSOConfig facebook
  SSOConfig google
  SSOConfig github

  static class SSOConfig {
    String clientId
    String clientSecret
    String redirectUri
  }
}

