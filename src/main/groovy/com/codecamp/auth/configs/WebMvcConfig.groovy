package com.codecamp.auth.configs


import com.codecamp.common.config.MvcConfig
import org.springframework.context.annotation.Configuration

@Configuration
class WebMvcConfig extends MvcConfig {
}
