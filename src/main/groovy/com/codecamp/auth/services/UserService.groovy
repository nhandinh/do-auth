package com.codecamp.auth.services

import com.codecamp.auth.dto.request.LoginRequest
import com.codecamp.auth.dto.request.SignUpRequest
import com.codecamp.auth.dto.response.AuthResponse
import com.codecamp.common.domains.User

interface UserService {

  void addUser(SignUpRequest signUpRequest)

  AuthResponse login(LoginRequest loginRequest)

  User findById(Long id)
}