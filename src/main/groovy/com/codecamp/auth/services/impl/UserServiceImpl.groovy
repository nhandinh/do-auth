package com.codecamp.auth.services.impl

import com.codecamp.auth.dto.request.LoginRequest
import com.codecamp.auth.dto.request.SignUpRequest
import com.codecamp.auth.dto.response.AuthResponse
import com.codecamp.auth.repositories.UserRepository
import com.codecamp.auth.services.UserService
import com.codecamp.common.domains.User
import com.codecamp.common.exception.BadRequestException
import com.codecamp.common.provider.TokenProvider
import com.codecamp.common.utils.AuthProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl implements UserService {

  @Autowired
  UserRepository userRepository

  @Autowired
  PasswordEncoder passwordEncoder

  @Autowired
  AuthenticationManager authenticationManager

  @Autowired
  TokenProvider tokenProvider

  @Override
  void addUser(SignUpRequest signUpRequest) {
    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      throw new BadRequestException('Email address already in use.')
    }
    User user = new User()
    user.name = signUpRequest.name
    user.email = signUpRequest.email
    user.password = passwordEncoder.encode(signUpRequest.password)
    user.provider = AuthProvider.LOCAL
    user.active = true
    userRepository.save(user)
  }

  @Override
  AuthResponse login(LoginRequest loginRequest) {
    Optional<User> userOptional = userRepository.findByEmail(loginRequest.email)
    userOptional.ifPresent {
      if(it.provider != AuthProvider.LOCAL) {
        throw new BadRequestException('Email address is logged with SSO.')
      }
    }
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            loginRequest.email,
            loginRequest.password
        )
    )
    SecurityContextHolder.getContext().setAuthentication(authentication)
    String token = tokenProvider.createToken(authentication)
    AuthResponse response = new AuthResponse()
    response.user = userOptional.get()
    response.token = token
    response
  }

  @Override
  User findById(Long id) {
    return this.userRepository.findById(id).orElseThrow({
      -> new IllegalAccessException('User not found')
    })

  }
}
