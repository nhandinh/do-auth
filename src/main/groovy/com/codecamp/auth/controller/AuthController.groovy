package com.codecamp.auth.controller

import com.codecamp.auth.dto.request.LoginRequest
import com.codecamp.auth.dto.request.SignUpRequest
import com.codecamp.auth.dto.response.AuthResponse
import com.codecamp.auth.services.UserService
import com.codecamp.common.dto.Result
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid

@RestController
@RequestMapping('/public')
class AuthController {

  @Autowired
  UserService userService

  @PostMapping('/login')
  ResponseEntity<AuthResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    AuthResponse response = this.userService.login(loginRequest)
    return ResponseEntity.ok(new Result<AuthResponse>().success(response))
  }

  @PostMapping('/signup')
  ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
    this.userService.addUser(signUpRequest)
    ResponseEntity.ok(new Result<String>().success('Register account successful'))
  }
}
