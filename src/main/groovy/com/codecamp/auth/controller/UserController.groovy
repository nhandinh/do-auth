package com.codecamp.auth.controller

import com.codecamp.auth.services.UserService
import com.codecamp.common.auth.annotation.CurrentUser
import com.codecamp.common.domains.User
import com.codecamp.common.dto.Result
import com.codecamp.common.utils.UserPrincipal
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController {

  @Autowired
  UserService userService

  @GetMapping('/user/me')
  ResponseEntity<Result<User>> getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
    User user = this.userService.findById(userPrincipal.id)
    return ResponseEntity.ok(new Result<User>().success(user))
  }
}
