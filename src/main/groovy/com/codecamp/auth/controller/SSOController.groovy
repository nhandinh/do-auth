package com.codecamp.auth.controller

import com.codecamp.auth.security.oauth2.user.OAuthUser
import com.codecamp.auth.security.sso.SSOConnectFactory
import com.codecamp.auth.security.sso.SSOException
import com.codecamp.auth.security.sso.SSOService
import com.codecamp.common.dto.Result
import com.codecamp.common.utils.AuthProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping(value = 'oauth2/sso')
class SSOController {

  @Autowired
  SSOConnectFactory ssoConnectFactory

  @Autowired
  SSOService ssoService

  @GetMapping(value = 'access_token', produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Result> getAccessToken(@RequestParam('code') String code) {
    try {
      String token = this.ssoConnectFactory.getSSOConnect(AuthProvider.FACEBOOK).getAccessToken(code)
      return ResponseEntity.ok(new Result<String>().success(token))
    } catch (SSOException e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Result<String>().error(e.getMessage()))
    }
  }

  @GetMapping(value = '{provider}/user_info', produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Result<OAuthUser>> getUserInfo(@PathVariable('provider') AuthProvider provider,
                                                @RequestParam('code') String code) {
    try {
      OAuthUser user = this.ssoService.signInSSO(provider, code)
      return ResponseEntity.ok(new Result<OAuthUser>().success(user))
    } catch (SSOException e) {
      Result<OAuthUser> err = new Result<>().error(e.getMessage())
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(err)
    }
  }
}
