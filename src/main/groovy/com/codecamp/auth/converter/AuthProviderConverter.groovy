package com.codecamp.auth.converter

import com.codecamp.common.utils.AuthProvider
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class AuthProviderConverter implements Converter<String, AuthProvider> {

  @Override
  AuthProvider convert(String source) {
    return AuthProvider.of(source)
  }

}
