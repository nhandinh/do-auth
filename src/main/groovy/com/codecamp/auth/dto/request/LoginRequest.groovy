package com.codecamp.auth.dto.request

import javax.validation.constraints.NotBlank

class LoginRequest {

  @NotBlank
  String email

  @NotBlank
  String password
}
