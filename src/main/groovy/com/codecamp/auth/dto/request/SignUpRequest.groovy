package com.codecamp.auth.dto.request

import javax.validation.constraints.NotBlank


class SignUpRequest {

  @NotBlank
  String name

  @NotBlank
  String email

  @NotBlank
  String password
}
