package com.codecamp.auth.dto.response


class TokenInfo {
  String token
  String email
  String imageUrl
}
