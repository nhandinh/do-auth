package com.codecamp.auth.dto.response

import com.codecamp.common.domains.User

class AuthResponse {

  String token
  User user
}
