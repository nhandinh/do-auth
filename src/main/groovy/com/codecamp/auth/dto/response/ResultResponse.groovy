package com.codecamp.auth.dto.response

class ResultResponse {
  boolean success
  String message

  ResultResponse(boolean success, String message) {
    this.success = success
    this.message = message
  }
}
