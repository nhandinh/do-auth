package com.codecamp.auth.security.oauth2.user

class OAuthFacebookUser extends OAuthUser {

  OAuthFacebookUser(Map<String, String> data) {
    name = (String) data.get('name')
    email = (String) data.get('email')
    id = (String) data.get('id')
    if (data.containsKey('picture')) {
      Map<String, Object> pictureObj = (Map<String, Object>) data.get('picture')
      if (pictureObj.containsKey('data')) {
        Map<String, Object> dataObj = (Map<String, Object>) pictureObj.get('data')
        if (dataObj.containsKey('url')) {
          imageUrl = (String) dataObj.get('url')
        }
      }
    }
  }
}
