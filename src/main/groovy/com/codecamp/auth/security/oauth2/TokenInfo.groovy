package com.codecamp.auth.security.oauth2

class TokenInfo {
  String token
  String type
  long expiresIn

  static TokenInfo create(String token, String type, long expiresIn) {
    TokenInfo info = new TokenInfo()
    info.token = token
    info.type = type
    info.expiresIn = expiresIn
    return info
  }
}
