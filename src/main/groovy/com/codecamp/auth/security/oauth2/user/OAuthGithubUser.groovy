package com.codecamp.auth.security.oauth2.user

class OAuthGithubUser extends OAuthUser {

  OAuthGithubUser(Map<String, String> data) {
    name = (String) data.get('name')
    email = (String) data.get('email')
    id = (String) data.get('id')
    imageUrl = (String) data.get('avatar_url')
  }
}
