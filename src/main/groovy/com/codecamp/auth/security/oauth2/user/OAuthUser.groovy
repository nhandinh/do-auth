package com.codecamp.auth.security.oauth2.user

abstract class OAuthUser {
  String id
  String email
  String imageUrl
  String name
  String token
}
