package com.codecamp.auth.security.sso

import com.codecamp.auth.utils.BeanConst
import com.codecamp.common.utils.AuthProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
class SSOConnectFactory {

  @Autowired
  @Qualifier(BeanConst.BEAN_CONNECT_GITHUB)
  SSOConnect ssoConnectGitHub

  @Autowired
  @Qualifier(BeanConst.BEAN_CONNECT_FACE)
  SSOConnect ssoConnectFacebook

  @Autowired
  @Qualifier(BeanConst.BEAN_CONNECT_GOOGLE)
  SSOConnect ssoConnectGoogle

  SSOConnect getSSOConnect(AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.GITHUB:
        return this.ssoConnectGitHub
      case AuthProvider.FACEBOOK:
        return this.ssoConnectFacebook
      case AuthProvider.GOOGLE:
        return this.ssoConnectGoogle
      default:
        throw new IllegalArgumentException("Provider ${authProvider.value} not support")
    }
  }
}
