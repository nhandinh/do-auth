package com.codecamp.auth.security.sso

import com.codecamp.auth.security.oauth2.user.OAuthUser
import com.codecamp.common.utils.AuthProvider

interface SSOService {

  OAuthUser signInSSO(AuthProvider provider, String code) throws SSOException

}