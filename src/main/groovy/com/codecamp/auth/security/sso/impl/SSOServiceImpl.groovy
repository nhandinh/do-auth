package com.codecamp.auth.security.sso.impl

import com.codecamp.auth.repositories.UserRepository
import com.codecamp.auth.security.oauth2.TokenInfo
import com.codecamp.auth.security.oauth2.user.OAuthUser
import com.codecamp.auth.security.sso.SSOConnect
import com.codecamp.auth.security.sso.SSOConnectFactory
import com.codecamp.auth.security.sso.SSOException
import com.codecamp.auth.security.sso.SSOService
import com.codecamp.common.domains.User
import com.codecamp.common.provider.TokenProvider
import com.codecamp.common.utils.AuthProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SSOServiceImpl implements SSOService {

  @Autowired
  SSOConnectFactory ssoConnectFactory

  @Autowired
  UserRepository userRepository

  @Autowired
  TokenProvider tokenProvider

  @Override
  OAuthUser signInSSO(AuthProvider provider, String code) throws SSOException {
    SSOConnect ssoConnect = this.ssoConnectFactory.getSSOConnect(provider)
    TokenInfo tokenInfo = ssoConnect.getAccessToken(code)
    OAuthUser userSSO = ssoConnect.getUserInfo(tokenInfo.token)
    Optional<User> user = this.userRepository.findByEmail(userSSO.email)
    if (user.present) {
      User u = user.get()
      u.imageUrl = userSSO.imageUrl
      u.name = userSSO.name
      u.providerId = userSSO.id
      u.active = true
      this.userRepository.save(u)
    } else {
      User u = new User()
      u.provider = provider
      u.providerId = userSSO.id
      u.email = userSSO.email
      u.name = userSSO.name
      u.imageUrl = userSSO.imageUrl
      u.active = true
      this.userRepository.save(u)
    }
    String localToken = this.tokenProvider.createToken(userSSO.email, tokenInfo.expiresIn)
    userSSO.token = localToken
    userSSO
  }


}
