package com.codecamp.auth.security.sso.impl

import com.codecamp.auth.configs.SSOAppConfig
import com.codecamp.auth.security.oauth2.TokenInfo
import com.codecamp.auth.security.oauth2.user.OAuthGithubUser
import com.codecamp.auth.security.oauth2.user.OAuthUser
import com.codecamp.auth.security.sso.SSOConnect
import com.codecamp.auth.security.sso.SSOException
import com.codecamp.auth.utils.BeanConst
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

@Component(BeanConst.BEAN_CONNECT_GITHUB)
@Slf4j
class SSOConnectGithub extends SSOConnectAbstract implements SSOConnect {

  static final String URL_GITHUB_ACCESS_TOKEN = 'https://github.com/login/oauth/access_token'
  static final String URL_GITHUB_USER_INFO = 'https://api.github.com/user'

  @Autowired
  SSOAppConfig ssoAppConfig

  Map<String, String> splitQuery(String query) {
    Map<String, String> query_pairs = new LinkedHashMap<String, String>()
    String[] pairs = query.split("&")
    for (String pair : pairs) {
      int idx = pair.indexOf("=")
      query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
          URLDecoder.decode(pair.substring(idx + 1), "UTF-8"))
    }
    return query_pairs
  }

  @Override
  TokenInfo getAccessToken(String code) {
    log.info("Get access token provider facebook")
    MultiValueMap<String, String> data = new LinkedMultiValueMap<>()
    data.add('client_id', this.ssoAppConfig.github.clientId)
    data.add('client_secret', this.ssoAppConfig.github.clientSecret)
    data.add('code', code)
    ResponseEntity<String> response = this.sendPostForm(URL_GITHUB_ACCESS_TOKEN, data)
    if (response.statusCode == HttpStatus.OK) {
      Map<String, String> responseData = this.splitQuery(response.body)
      if (responseData.get('access_token')) {
        return TokenInfo.create(responseData.get('access_token'), responseData.get('token_type'), 0)
      } else {
        throw new SSOException("Get access token error ${responseData.get('error_description')}")
      }
    } else {
      throw new SSOException('Can\'t get access token')
    }
  }

  @Override
  OAuthUser getUserInfo(String accessToken) throws SSOException {
    HttpHeaders headers = new HttpHeaders()
    headers.add('Authorization', "token ${accessToken}")
    ResponseEntity<Map<String, String>> res = this.sendGet(URL_GITHUB_USER_INFO, headers)
    if (res.statusCode == HttpStatus.OK) {
      OAuthUser oAuthUser = new OAuthGithubUser(res.body)
      return oAuthUser
    } else {
      throw new SSOException('Can\'t get user info')
    }
  }
}
