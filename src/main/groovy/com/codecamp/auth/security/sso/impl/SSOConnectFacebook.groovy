package com.codecamp.auth.security.sso.impl

import com.codecamp.auth.configs.SSOAppConfig
import com.codecamp.auth.security.oauth2.TokenInfo
import com.codecamp.auth.security.oauth2.user.OAuthFacebookUser
import com.codecamp.auth.security.oauth2.user.OAuthUser
import com.codecamp.auth.security.sso.SSOConnect
import com.codecamp.auth.security.sso.SSOException
import com.codecamp.auth.utils.BeanConst
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Slf4j
@Component(BeanConst.BEAN_CONNECT_FACE)
class SSOConnectFacebook extends SSOConnectAbstract implements SSOConnect {

  static final URL_ACCESS_TOKEN = 'https://graph.facebook.com/v5.0/oauth/access_token?'
  static final URL_USER_INFO = 'https://graph.facebook.com/me?'

  @Autowired
  SSOAppConfig ssoAppConfig

  @Override
  TokenInfo getAccessToken(String code) throws SSOException {
    SSOAppConfig.SSOConfig config = ssoAppConfig.facebook
    String url = "${URL_ACCESS_TOKEN}client_id=${config.clientId}&client_secret=${config.clientSecret}&code=${code}&redirect_uri=${config.redirectUri}"
    HttpHeaders headers = new HttpHeaders()
    headers.setAccept([MediaType.APPLICATION_JSON])
    ResponseEntity<Map<String, String>> res = this.sendGet(url, headers)
    if (res.statusCode == HttpStatus.OK) {
      Map<String, String> data = res.body
      return TokenInfo.create((String) data.get('access_token'), '', 0)
    } else {
      throw new SSOException("Can't get access token")
    }
  }

  @Override
  OAuthUser getUserInfo(String accessToken) throws SSOException {
    String url = "${URL_USER_INFO}fields=id,name,email,picture&access_token=${accessToken}"
    HttpHeaders headers = new HttpHeaders()
    headers.setAccept([MediaType.APPLICATION_JSON])
    ResponseEntity<Map<String, String>> res = this.sendGet(url, headers)
    if (res.statusCode == HttpStatus.OK) {
      println res.body
      return new OAuthFacebookUser(res.body)
    } else {
      throw new SSOException('Can\'t get access token')
    }
  }
}
