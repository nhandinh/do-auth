package com.codecamp.auth.security.sso

import com.codecamp.auth.security.oauth2.TokenInfo
import com.codecamp.auth.security.oauth2.user.OAuthUser

interface SSOConnect {

  TokenInfo getAccessToken(String code) throws SSOException

  OAuthUser getUserInfo(String accessToken) throws SSOException

}