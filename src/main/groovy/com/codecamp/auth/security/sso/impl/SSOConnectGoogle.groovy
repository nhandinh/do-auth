package com.codecamp.auth.security.sso.impl

import com.codecamp.auth.configs.SSOAppConfig
import com.codecamp.auth.security.oauth2.TokenInfo
import com.codecamp.auth.security.oauth2.user.OAuthUser
import com.codecamp.auth.security.sso.SSOConnect
import com.codecamp.auth.security.sso.SSOException
import com.codecamp.auth.utils.BeanConst
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Slf4j
@Component(BeanConst.BEAN_CONNECT_GOOGLE)
class SSOConnectGoogle extends SSOConnectAbstract implements SSOConnect {

  @Autowired
  SSOAppConfig ssoAppConfig

  @Override
  TokenInfo getAccessToken(String code) throws SSOException {
    return null
  }

  @Override
  OAuthUser getUserInfo(String accessToken) throws SSOException {
    return null
  }
}
