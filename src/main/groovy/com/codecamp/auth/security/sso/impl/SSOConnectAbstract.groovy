package com.codecamp.auth.security.sso.impl

import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate

abstract class SSOConnectAbstract {

  RestTemplate restTemplate

  HttpHeaders postFormHeader

  HttpHeaders jsonHeader

  SSOConnectAbstract() {
    this.restTemplate = new RestTemplate()
    this.setPostHeader()
  }

  void setPostHeader() {
    this.postFormHeader = new HttpHeaders()
    this.postFormHeader.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
  }

  ResponseEntity<String> sendPostForm(String url, MultiValueMap<String, String> data) {
    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(data, this.postFormHeader)
    this.restTemplate.exchange(url, HttpMethod.POST, entity, String.class)
  }

  ResponseEntity<Map<String, Object>> sendGet(String url, HttpHeaders headers) {
    ParameterizedTypeReference<HashMap<String, Object>> responseType =
        new ParameterizedTypeReference<HashMap<String, Object>>() {}
    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(null, headers)
    this.restTemplate.exchange(url, HttpMethod.GET, entity, responseType)
  }

}
