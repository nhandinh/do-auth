package com.codecamp.auth.utils

import org.springframework.util.SerializationUtils

import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

final class CookieUtils {

  static Optional<Cookie> getCookie(HttpServletRequest request, String name) {
    Cookie[] cookies = request.getCookies()
    if (cookies != null && cookies.length > 0) {
      for (Cookie cookie : cookies) {
        if (cookie.name == name) {
          return Optional.of(cookie);
        }
      }
    }
    Optional.empty()
  }

  static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
    Cookie cookie = new Cookie(name, value)
    cookie.path = '/'
    cookie.httpOnly = true
    cookie.maxAge = maxAge
    response.addCookie(cookie)
  }

  static void deleteCookie(HttpServletRequest request, HttpServletResponse response, String name) {
    Cookie[] cookies = request.cookies
    if (cookies != null && cookies.length > 0) {
      for (Cookie cookie : cookies) {
        if (cookie.name == name) {
          cookie.value = ''
          cookie.path = '/'
          cookie.maxAge = 0
          response.addCookie(cookie)
        }
      }
    }
  }

  static String serialize(Object object) {
    Base64.getUrlEncoder()
        .encodeToString(SerializationUtils.serialize(object))
  }

  static <T> T deserialize(Cookie cookie, Class<T> cls) {
    cls.cast(SerializationUtils.deserialize(
        Base64.getUrlDecoder().decode(cookie.getValue())));
  }
}
