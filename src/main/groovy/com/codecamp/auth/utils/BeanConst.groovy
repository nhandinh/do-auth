package com.codecamp.auth.utils

final class BeanConst {

  static final String BEAN_CONNECT_GITHUB = 'BEAN_CONNECT_GITHUB'
  static final String BEAN_CONNECT_FACE = 'BEAN_CONNECT_FACE'
  static final String BEAN_CONNECT_GOOGLE = 'BEAN_CONNECT_GOOGLE'
}
