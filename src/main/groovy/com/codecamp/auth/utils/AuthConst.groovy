package com.codecamp.auth.utils

final class AuthConst {

  static final String[] WHITE_LIST_URLS = [
      '/',
      '/error',
      '/favicon.ico',
      '/**/*.png',
      '/**/*.gif',
      '/**/*.svg',
      '/**/*.jpg',
      '/**/*.html',
      '/**/*.css',
      '/**/*.js'
  ]

  static final String[] APP_PERMIT_ALL = [
      '/public/**',
      '/oauth2/**'
  ]
}
