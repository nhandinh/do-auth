package com.codecamp.auth

import com.codecamp.auth.configs.AuthAppProperties
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

/**
 * Create by dhnhan on 13/12/2019
 * Authentication Service
 */

@SpringBootApplication
@EnableEurekaClient
@EnableConfigurationProperties(AuthAppProperties.class)
@EntityScan('com.codecamp.common.domains')
class AuthApplication {

  static void main(String[] args) {
    SpringApplication.run AuthApplication, args
  }
}
